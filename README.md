# stock-frontend

The frontend integration for the Payconiq Stock Manager.

The WAR file generated from the build can be deployed on Apache Tomcat and access on the browser with the URL: [link](http://localhost:8080/stocks).

Login with any emailaddress and password. Tokens are automatically served by the backend application.

