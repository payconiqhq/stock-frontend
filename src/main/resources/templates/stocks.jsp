<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payconiq Stock Listing - Welcome</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/styles.css" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous" />
        <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    </head>
    <body>

        <div class="container">
            <div class="row margin-top-150px">                
                <div class="col-md-12">  
                    <h3 class="centers">Payconiq Current Stock Listings</h3>
                    <div class="padding-50px bordered margin-top-20px">
                        <table class="table table-striped">
                            <tr>
                                <th class="centers">ID</th>
                                <th class="centers">Name</th>
                                <th class="centers">Price</th>
                                <th class="centers">Last Update Date/Time</th>
                            </tr>
                            <tr th:each="stock:${stocks}">
                                <td class="centers" th:text="${stock.id}" />
                                <td class="centers" th:text="${stock.name}" />
                                <td class="centers" th:text="${stock.currentPrice}" />
                                <td class="centers" th:text="${stock.lastUpdate}" />
                            </tr>                                    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
