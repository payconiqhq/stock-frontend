<html lang="en" xmlns:th="http://www.thymeleaf.org">
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payconiq Stock Listing - Login</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/styles.css" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous" />
        <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    </head>
    
    <body>
        <div class="container">
            <div class="row margin-top-150px">
                <div class="col-md-4"></div>
                <div class="col-md-4">  
                    <h3 class="centers">Welcome to <br />Payconiq Stock Manager</h3>
                    <div class="padding-50px bordered margin-top-20px">
                        <form method="post" autocomplete="off" action="login">
                            <div class="row">
                                <div class="col-md-12">Email address</div>
                                <div class="col-md-12 margin-top-10px">
                                    <input type="text" class="form-control" placeholder="Email address" required="true" name="emailaddress"/>
                                </div>
                            </div>
                            <div class="row margin-top-20px">
                                <div class="col-md-12">Password</div>
                                <div class="col-md-12 margin-top-10px">
                                    <input type="password" class="form-control" placeholder="Password" required="true" name="password"/>
                                </div>
                            </div>                    
                            <div class="row margin-top-10px">
                                <div class="col-md-12 margin-top-10px centers">
                                    <button class="btn btn-primary">Login</button>
                                </div>
                            </div>                                            
                            <div class="row margin-top-10px">
                                <div class="col-md-12">
                                    <div class="ui-messages"><th:text="${messages}" /></div>
                                </div>
                            </div>                        
                        </form>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>

    </body>
</html>
