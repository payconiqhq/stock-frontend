/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stock.frontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author ceowit
 */
@SpringBootApplication
public class StockFrontendApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockFrontendApplication.class, args);
    }

}
