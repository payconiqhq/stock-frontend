/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stock.frontend.operation;

import java.util.List;
import com.payconiq.stock.frontend.exchange.Stock;

/**
 *
 * @author ceowit
 */
public interface BackendOperation {
    
    String login(String emailaddress,String password) throws Exception;
    
    List<Stock> stocks(String authorization) throws Exception;
    
}
