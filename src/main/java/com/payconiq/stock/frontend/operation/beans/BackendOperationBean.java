/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stock.frontend.operation.beans;

import com.payconiq.stock.frontend.exchange.AbstractResponse;
import com.payconiq.stock.frontend.exchange.Stock;
import com.payconiq.stock.frontend.operation.BackendOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author ceowit
 */
@Component
public class BackendOperationBean implements BackendOperation {

    @Autowired
    RestTemplate restTemplate;

    @Value("${backend.url}")
    private String backendurl;
    
    /**
     * 
     * @param emailaddress
     * @param password
     * @return
     * @throws Exception 
     */
    @Override
    public String login(String emailaddress, String password) throws Exception {
        String url = UriComponentsBuilder.fromUriString(backendurl.concat("/authorize")).queryParam("emailaddress", emailaddress).queryParam("password", password).build().toString();
        ResponseEntity<AbstractResponse<String>> responseEntity = this.restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<AbstractResponse<String>>() {
        });
        if (responseEntity == null || responseEntity.getStatusCode() != HttpStatus.OK) {
            throw new Exception("Unauthorized access");
        }
        return responseEntity.getBody().getData();
    }

    /**
     * 
     * @param authorization
     * @return
     * @throws Exception 
     */
    @Override
    public List<Stock> stocks(String authorization) throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", authorization);
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        String url = UriComponentsBuilder.fromUriString(backendurl).build().toString();
        ResponseEntity<AbstractResponse<List<Stock>>> responseEntity = this.restTemplate.exchange(url, HttpMethod.GET, httpEntity, new ParameterizedTypeReference<AbstractResponse<List<Stock>>>() {
        });
        if (responseEntity == null || responseEntity.getStatusCode() != HttpStatus.OK || responseEntity.getBody() == null) {
            throw new Exception("Unauthorized access");
        }
        return responseEntity.getBody().getData();
    }

}
