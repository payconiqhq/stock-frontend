/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stock.frontend.controllers;

import com.payconiq.stock.frontend.exchange.Login;
import com.payconiq.stock.frontend.operation.BackendOperation;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author ceowit
 */
@Controller
public class FormController {

    @Autowired
    BackendOperation backendOperation;

    @PostMapping(value = "/login")
    public String login(Login login, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
        try {
            String token = this.backendOperation.login(login.getEmailaddress(), login.getPassword());
            if (token != null || token.length() > 0) {
                Cookie cookie = new Cookie("token", token);
                cookie.setMaxAge(3600 * 24);
                cookie.setPath(request.getSession().getServletContext().getContextPath());
                response.addCookie(cookie);
                redirectAttributes.addFlashAttribute("messages", "Login successful");
            } else {
                redirectAttributes.addFlashAttribute("messages", "Login failed");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            redirectAttributes.addFlashAttribute("messages", ex.getMessage());
        }
        return "redirect:/";
    }

}
