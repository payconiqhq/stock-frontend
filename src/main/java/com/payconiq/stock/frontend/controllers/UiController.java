/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stock.frontend.controllers;

import com.payconiq.stock.frontend.operation.BackendOperation;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author ceowit
 */
@Controller
public class UiController {

    @Autowired
    BackendOperation backendOperation;

    @GetMapping(value = "/")
    public String index(HttpServletRequest request, HttpServletResponse response, Model model) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("token") && cookie.getValue().length() > 0) {
                    try {
                        model.addAttribute("stocks", backendOperation.stocks(cookie.getValue()));
                    } catch (Exception ex) {
                        model.addAttribute("messages", "Unable to fetch stocks at this time");
                    }
                    return "stocks";
                }
            }
        }
        return "index";
    }

    @GetMapping(value = "/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response, Model model) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("token") && cookie.getValue().length() > 0) {
                    cookie.setValue("");
                    cookie.setMaxAge(-1);
                    response.addCookie(cookie);
                }
            }
        }
        return "index";
    }

}
